package grofers.gMethods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import btac.sa.lib.FilesAndFolders;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class GrofersMethods extends SelectBrowser {

	public static WebDriver driver;
	public GrofersMethods(WebDriver driver){
		this.driver=driver;
	}

	static Workbook wrk1;
	static Sheet sheet1;
	static Cell colRow;
	
	//web elements ...........................................................
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[2]/header/div[2]/div/div[1]/input")
	public static WebElement textField;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[2]/header/div[2]/div/div[1]/button")
	public static WebElement searchBtn;
	
	//web elements of result field...............................................
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[1]/div/div[4]/div")
	public static WebElement r1Field;//result fields 1 to 8.......
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[2]/div/div[4]/div")
	public static WebElement r2Field;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[3]/div/div[4]/div")
	public static WebElement r3Field;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[4]/div/div[4]/div")
	public static WebElement r4Field;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[5]/div/div[4]/div")
	public static WebElement r5Field;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[6]/div/div[4]/div")
	public static WebElement r6Field;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[7]/div/div[4]/div")
	public static WebElement r7Field;
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[8]/div/div[4]/div")
	public static WebElement r8Field;
	//.//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[1]/nav/ul/li[2]/a/span
	@FindBy(xpath=".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[1]/nav/ul/li[2]/a/span")
	public static WebElement assertField;
	//Only to search.......................................................
	public static void search(String str) throws Exception
	{	
		System.out.println("-----------------------------------Search started -----------------------------------");
		
		Thread.sleep(3000);
		WebCommonMethods.callingImplicitSleep();
		
		
		textField.clear();
		textField.sendKeys(str);
		Thread.sleep(5000);
		
		searchBtn.click();
		Thread.sleep(5000);
		WebCommonMethods.callingImplicitSleep();
		  //sending userName, password

//		Assert.assertEquals(str2, record[7].getContents());
		System.out.println("-----------------------------------Search successful -----------------------------------");
	}
	
	//to assert the results...................
	public static void resultAssert(String str) throws Exception
	{
		System.out.println("----------------------------------- result generation -----------------------------------");
		String assterStr="Apple";
		//Assert.assertEquals(assterStr,assertField.getText());
		System.out.println("------------------------Navigation Successful --------------------");
		
		String res1=r1Field.getText();
		System.out.println("record 1: "+res1);
		String res2=r2Field.getText();
		System.out.println("record 2: "+res2);
		String res3=r3Field.getText();
		System.out.println("record 3: "+res3);
		String res4=r4Field.getText();
		System.out.println("record 4: "+res4);
		String res5=r5Field.getText();
		String res6=r6Field.getText();
		String res7=r7Field.getText();
		String res8=r8Field.getText();
		
		
		System.out.println("record 4: "+res4);
		System.out.println("record 5: "+res5);
		System.out.println("record 6: "+res6);
		System.out.println("record 7: "+res7);
		System.out.println("record 8: "+res8);
	}
}
